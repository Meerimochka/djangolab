from django.conf.urls import url
from django.contrib import admin
from django.template.context_processors import static
from mylab import settings
from mylab.views import IndexView, Sms2View, CalculateView

urlpatterns = [

    url(r'^$', IndexView.as_view()),
    url(r'^sms2/', Sms2View.as_view()),
    url(r'^calculate/', CalculateView.as_view()),
    url(r'^admin/', admin.site.urls),
]