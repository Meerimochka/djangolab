from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"


class Sms2View(TemplateView):
    template_name = "sms2.html"


class CalculateView(TemplateView):
    template_name = "calculate.html"
